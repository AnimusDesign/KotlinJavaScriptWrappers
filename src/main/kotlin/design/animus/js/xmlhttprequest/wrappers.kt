package design.animus.js.xmlhttprequest

import kotlinx.serialization.KSerializer
import kotlinx.serialization.SerialContext
import kotlinx.serialization.json.JSON
import org.w3c.xhr.XMLHttpRequest
import kotlin.coroutines.experimental.Continuation
import kotlin.coroutines.experimental.suspendCoroutine
import kotlin.reflect.KClass

/**
 * An enum representing supported HTTP verbs.
 */
enum class HTTPVerbs {
    POST,
    GET,
    PUT,
    UPDATE,
    DELETE
}

/**
 * Utilized by all HTTP responses. Gets the response as a string and amends to the coroutine
 */
fun statusHandler(xhr: XMLHttpRequest, coroutineContext: Continuation<String>) {
    if (xhr.readyState == XMLHttpRequest.DONE) {
        if (xhr.status / 100 == 2) {
            coroutineContext.resume(xhr.response as String)
        } else {
            coroutineContext.resumeWithException(RuntimeException("HTTP error: ${xhr.status}"))
        }
    } else {
        null
    }
}

/**
 * Base wrapper for the GET verb
 */
suspend fun httpGet(url: String): String = suspendCoroutine { c ->
    val xhr = XMLHttpRequest()
    xhr.onreadystatechange = { _ -> statusHandler(xhr, c) }
    xhr.open("GET", url)
    xhr.send()
}

/**
 * Base wrapper for PUT or POST verb.
 * *Need to add support outside of json uplods.*
 */
suspend fun httpPutOrPost(url: String, data: String, httpVerb: HTTPVerbs): String = suspendCoroutine { c ->
    val xhr = XMLHttpRequest()
    xhr.onreadystatechange = { _ -> statusHandler(xhr, c) }
    xhr.open(httpVerb.name, url, true)
    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8")
    xhr.send(data)
}

/**
 * Wrapper for uploading items, and casting to native Kotlin classes.
 */
suspend inline fun <reified S : Any, reified R : Any> uploadBase(url: String,
                                                                 httpVerb: HTTPVerbs,
                                                                 data: S, dataClazz: KClass<S>,
                                                                 dataSerializer: KSerializer<S>,
                                                                 responseClazz: KClass<R>,
                                                                 responseSerializer: KSerializer<R>,
                                                                 debug: Boolean = true): R {
    val sendJsonContext = SerialContext()
            .apply { registerSerializer(dataClazz, dataSerializer) }
    val jsonString = JSON(context = sendJsonContext).stringify(data)
    val response = httpPutOrPost(url, jsonString, httpVerb)
    val responseJsonContext = SerialContext()
            .apply { registerSerializer(responseClazz, responseSerializer) }
    return JSON(context = responseJsonContext).parse(response)
}

/**
 * Slim wrapper simply setting HTTPVerb to PUT
 */
suspend inline fun <reified S : Any, reified R : Any> putBase(url: String,
                                                              data: S,
                                                              dataClazz: KClass<S>,
                                                              dataSerializer: KSerializer<S>,
                                                              responseClazz: KClass<R>,
                                                              responseSerializer: KSerializer<R>,
                                                              debug: Boolean = true): R {

    return uploadBase(url, HTTPVerbs.PUT, data, dataClazz, dataSerializer, responseClazz, responseSerializer)
}

/**
 * Slim wrapper simply setting HTTPVerb to POST
 */
suspend inline fun <reified S : Any, reified R : Any> postBase(url: String,
                                                               data: S, dataClazz: KClass<S>,
                                                               dataSerializer: KSerializer<S>,
                                                               responseClazz: KClass<R>,
                                                               responseSerializer: KSerializer<R>,
                                                               debug: Boolean = true): R {

    return uploadBase(url, HTTPVerbs.POST, data, dataClazz, dataSerializer, responseClazz, responseSerializer)
}


suspend inline fun <reified R : Any> getBase(url: String, clazz: KClass<R>, serializer: KSerializer<R>, debug: Boolean = true): R {
    if (debug) console.log("Fetching for url $url, and class of ${clazz.simpleName}")
    val rawData = httpGet(url)
    console.log(rawData)
    val jsonContext = SerialContext()
            .apply { registerSerializer(clazz, serializer) }
    val parsed = JSON(context = jsonContext).parse<R>(rawData)
    if (debug) console.log("Received a response of:\n $parsed")
    return parsed
}
